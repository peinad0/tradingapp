

import os
from tradingapp.settings.base import *


DEBUG = True
SECRET_KEY = os.environ.get('SECRET_KEY', '')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

CORS_ORIGIN_ALLOW_ALL = True