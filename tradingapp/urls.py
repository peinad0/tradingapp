

from django.contrib import admin
from django.urls import include, path


urlpatterns = [
    path('', include('trades_web_view.urls')),
    path('admin/', admin.site.urls),
    path('api/v1/trades/', include('api_trades.urls')),
    path('api/v1/rates/', include('api_rates.urls')),
]
