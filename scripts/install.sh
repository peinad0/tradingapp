#!/usr/bin/env bash
echo "*********** INSTALLING DEPENDENCIES ***********"

echo ">>> Updating system"
apt-get update -qy

echo ">>> Installing python dependencies"
apt-get -y install python3-pip
apt-get -y install python3-dev python3-setuptools
pip install --upgrade pip

echo ">>> Installing project dependencies"
pip install pipenv
pipenv install

echo ">>> Installed packages"

pip freeze