

from django.test import TestCase

from rest_framework.test import APIRequestFactory

from .. import views


class CurrencyRatesViewTest(TestCase):
    """
    Test suite for `CurrencyRatesView` view.
    """

    @classmethod
    def setUpTestData(cls):
        """
        Set up objects used by all tests.
        """
        cls.currency = 'usd'

    def setUp(self):
        """
        Set test objects and variables before each test.
        """
        self.request = APIRequestFactory().get("")
        self.detail = views.CurrencyRatesView.as_view({'get': 'retrieve'})

    def test_api_endpoint_is_active(self):
        response = self.detail(self.request, currency=self.currency)
        self.assertEqual(response.status_code, 200)

    def test_get_dollar_rates_data_types(self):
        response = self.detail(self.request, currency=self.currency)
        rates = response.data['rates']
        self.assertTrue(isinstance(rates, dict))
        for key, value in rates.items():
            self.assertTrue(isinstance(key, str))
            self.assertTrue(isinstance(value, float))
            self.assertTrue(value > 0)
