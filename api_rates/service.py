
import requests


class BaseCurrencyRateProvider(object):
    """Rate provider interface."""

    @classmethod
    def get_rate(cls, base, requested):
        """Get `requested` currency rate using `base` as the base currency."""
        raise NotImplementedError

    @classmethod
    def get_all_rates(cls, base):
        """Get all currency rates using `base` as the base currency."""
        raise NotImplementedError


class FixerCurrencyRateProvider(BaseCurrencyRateProvider):
    """Rate provider wrapping the fixer.io api."""
    SERVICE_BASE_URL = 'https://api.fixer.io'

    @classmethod
    def get_rate(cls, base, requested):
        """Get Fixer `requested` rate using `base` as the base currency."""
        params = {'base': base, 'symbols': requested}
        url = '{}/latest'.format(cls.SERVICE_BASE_URL)
        response = requests.get(url, params=params)
        return response.json().get('rates').get(requested)

    @classmethod
    def get_all_rates(cls, base):
        """Get Fixer rates using `base` as the base currency."""
        params = {'base': base}
        url = '{}/latest'.format(cls.SERVICE_BASE_URL)
        response = requests.get(url, params=params)
        return response.json().get('rates')
