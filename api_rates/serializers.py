

from rest_framework import serializers


class CurrencySerializer(serializers.Serializer):
    """
    Serializes and validates request parameters
    """
    currency = serializers.CharField(max_length=20)
