

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(r'',
                views.CurrencyRatesView, base_name='v1rates')
urlpatterns = router.urls
