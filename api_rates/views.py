

import requests

from rest_framework import serializers as rfserializers
from rest_framework.exceptions import APIException, NotFound
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from . import serializers
from . import service


class CurrencyRatesView(ViewSet):
    """
    View to list currency rates based on a given currency.
    """
    serializer_class = serializers.CurrencySerializer
    lookup_field = 'currency'
    renderer_classes = (JSONRenderer, )

    def retrieve(self, request, currency):
        """
        Returns a list of currency rates based on a given currency.
        """
        serializer = serializers.CurrencySerializer(
            data={'currency': currency})
        if not serializer.is_valid():
            raise rfserializers.ValidationError(serializer.errors)
        valid_currency = serializer.validated_data['currency']
        try:
            rates = service.FixerCurrencyRateProvider.get_all_rates(
                valid_currency)
        except requests.exceptions.RequestException:
            raise APIException(
                'Service temporarily unavailable, try again later.')
        if rates is None:
            raise NotFound(
                'Cannot find {} currency rates'.format(valid_currency))
        content = {'rates': rates}
        return Response(content)
