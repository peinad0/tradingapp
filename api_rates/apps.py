from django.apps import AppConfig


class ApiRatesConfig(AppConfig):
    name = 'api_rates'
