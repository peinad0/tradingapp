

from rest_framework import mixins
from rest_framework import viewsets

from . import models
from . import serializers


class ListCreateViewSet(mixins.CreateModelMixin,
                        mixins.ListModelMixin,
                        viewsets.GenericViewSet):
    """
    Viewset providing `list()` and `create()` actions.
    """
    pass


class TradesListCreateView(ListCreateViewSet):
    """
    View to list all `Trade` and create new ones.
    """
    queryset = models.Trade.objects.all()
    serializer_class = serializers.TradeSerializer

    def perform_create(self, serializer):
        trade = serializer.validated_data
        rate = trade.get('rate')
        sell_amount = trade.get('sell_amount')
        serializer.save(buy_amount=sell_amount * rate)
