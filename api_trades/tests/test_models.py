

import datetime

from django.test import TestCase
from django.utils import timezone

from .. import models


class TradeModelTest(TestCase):
    """
    Test suite for `Trade` model
    """

    @classmethod
    def setUpTestData(cls):
        """
        Set up objects used by all tests.
        """
        date = timezone.make_aware(
            datetime.datetime(1994, 8, 8, 12, 30, 45, 0))
        models.Trade.objects.create(
            id='TRAAAAAA1',
            sell_currency='EUR',
            buy_currency='USD',
            sell_amount=100,
            buy_amount=120,
            rate=1.2,
            booked_date=date)

    def setUp(self):
        """
        Set test objects and variables before each test.
        """
        self.date = timezone.make_aware(
            datetime.datetime(1994, 8, 8, 12, 30, 45, 0))
        self.correct_trade = models.Trade(
            id='TRAAAAAA2',
            sell_currency='EUR',
            buy_currency='USD',
            sell_amount=100,
            buy_amount=120,
            rate=1.2,
            booked_date=self.date)
        self.incorrect_trade = models.Trade(
            id='TRA1B2C3D',
            sell_currency='EUR',
            buy_currency='USD',
            sell_amount=100,
            buy_amount=220,
            rate=1,
            booked_date=self.date)

    def test_string_representation(self):
        representation = ('TRAAAAAA2: 100 EUR to 120 USD '
                          '(rate=1.2, date=1994-08-08 12:30:45+00:00)')
        self.assertEqual(str(self.correct_trade), representation)

    def test_is_buy_amount_valid(self):
        self.assertTrue(self.correct_trade.is_buy_amount_valid())
        self.assertFalse(self.incorrect_trade.is_buy_amount_valid())

    def test_model_saves_to_db(self):
        old_count = models.Trade.objects.count()
        self.correct_trade.save()
        new_count = models.Trade.objects.count()
        self.assertNotEqual(old_count, new_count)
        self.assertEquals(old_count, 1)
        self.assertEquals(new_count, 2)

    def test_generate_id(self):
        id = models.Trade.generate_id(prefix='P', suffix='S', length=10)
        self.assertTrue(id.startswith('P'))
        self.assertTrue(id.endswith('S'))
        self.assertEquals(len(id), 10)
