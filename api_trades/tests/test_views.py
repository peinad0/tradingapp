
import json
import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status

from .. import models


class TradeListCreateViewTest(TestCase):
    """
    Test suite for `Trade` list and create views
    """

    @classmethod
    def setUpTestData(cls):
        """
        Set up objects used by all tests.
        """
        date = timezone.make_aware(
            datetime.datetime(1994, 8, 8, 12, 30, 45, 0))
        models.Trade.objects.create(
            id='TRAAAAAA1',
            sell_currency='EUR',
            buy_currency='USD',
            sell_amount=100,
            buy_amount=120,
            rate=1.2,
            booked_date=date)

    def setUp(self):
        """
        Set test objects and variables before each test.
        """
        self.client = APIClient()
        self.valid_payload = {
            'sell_currency': 'USD',
            'sell_amount': 100,
            'buy_currency': 'EUR',
            'buy_amount': 90,
            'rate': 0.9
        }
        self.invalid_payload = {
            'sell_currency': '',
            'sell_amount': 100,
            'buy_currency': 'EUR',
            'buy_amount': 90,
            'rate': 1.1
        }
        self.invalid_datatype_payload = {
            'sell_currency': 23,
            'sell_amount': 100,
            'buy_currency': 'EUR',
            'buy_amount': 90,
            'rate': 1.1
        }

    def test_api_endpoint_is_active(self):
        response = self.client.get(
            reverse('v1trades-list'))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_api_endpoint_lists_one_trade(self):
        response = self.client.get(
            reverse('v1trades-list'))
        self.assertEquals(len(response.data), 1)

    def test_create_valid_trade(self):
        response = self.client.post(
            reverse('v1trades-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_trade_with_empty_field(self):
        response = self.client.post(
            reverse('v1trades-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_trade_with_invalid_data_type(self):
        response = self.client.post(
            reverse('v1trades-list'),
            data=json.dumps(self.invalid_datatype_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
