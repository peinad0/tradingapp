from __future__ import unicode_literals

from django.apps import AppConfig


class ApiTradesConfig(AppConfig):
    name = 'api_trades'
