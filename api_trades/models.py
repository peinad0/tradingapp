
import string
import random

from django.db import models
from django.utils import timezone
from django.core.validators import MinLengthValidator, ValidationError


ID_LEN = 7
ID_PREFIX = 'TR'
CURRENCY_ACRONYM_MAX_LEN = 20
CURRENCY_ACRONYM_MIN_LEN = 3


class Trade(models.Model):
    """
    Class representing the trade model.
    """

    id = models.CharField(max_length=ID_LEN, primary_key=True, blank=True,
                          validators=[MinLengthValidator(ID_LEN)])
    sell_currency = models.CharField(
        max_length=CURRENCY_ACRONYM_MAX_LEN,
        validators=[MinLengthValidator(CURRENCY_ACRONYM_MIN_LEN)])
    sell_amount = models.FloatField()
    buy_currency = models.CharField(
        max_length=CURRENCY_ACRONYM_MAX_LEN,
        validators=[MinLengthValidator(CURRENCY_ACRONYM_MIN_LEN)])
    buy_amount = models.FloatField()
    rate = models.FloatField()
    booked_date = models.DateTimeField(default=None, blank=True)

    @staticmethod
    def generate_id(prefix='', suffix='', length=7,
                    chars=string.ascii_uppercase + string.digits):
        """
        Generates a random id.
        """
        prefix_len, suffix_len = (len(prefix), len(suffix))
        if prefix_len + suffix_len >= length:
            raise ValueError(
                'prefix + suffix are equals or larger than length')
        id = ''.join(random.choice(chars)
                     for _ in range(length - prefix_len - suffix_len))
        return ''.join((prefix, id, suffix))

    def is_buy_amount_valid(self):
        return self.sell_amount * self.rate == self.buy_amount

    def save(self, *args, **kwargs):
        """
        Save trade in the database.
        """
        if not self.id:
            self.id = self.generate_id(prefix=ID_PREFIX)
            while Trade.objects.filter(id=self.id).exists():
                self.id = self.generate_id(prefix=ID_PREFIX)
            self.booked_date = timezone.now()
        if self.is_buy_amount_valid():
            return super(Trade, self).save(*args, **kwargs)
        else:
            raise ValidationError(('buy amount != sell_amount * rate'), code='error1')

    def __str__(self):
        """
        Return human readable trade representation.
        """
        return '{}: {} {} to {} {} (rate={}, date={})'.format(
            self.id,
            self.sell_amount,
            self.sell_currency,
            self.buy_amount,
            self.buy_currency,
            self.rate,
            self.booked_date,
        )
