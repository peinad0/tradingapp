

from rest_framework import serializers

from . import models


class TradeSerializer(serializers.ModelSerializer):
    """
    Serializes and validates model fields
    """
    class Meta:
        model = models.Trade
        fields = ('sell_currency',
                  'buy_currency',
                  'sell_amount',
                  'buy_amount',
                  'rate',
                  'booked_date')
        read_only_fields = ('id', 'booked_date')
