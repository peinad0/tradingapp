

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(r'', views.TradesListCreateView, base_name='v1trades')
urlpatterns = router.urls
