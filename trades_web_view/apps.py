from django.apps import AppConfig


class TradesWebViewConfig(AppConfig):
    name = 'trades_web_view'
