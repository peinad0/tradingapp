export class Trade {
  sell_currency: string = '';
  buy_currency: string = '';
  sell_amount: number;
  buy_amount: number;
  rate: number;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}




