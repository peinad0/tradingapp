import { Injectable } from '@angular/core';
import { Trade } from './trade';
import { Http, Response, HttpModule } from '@angular/http';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class TradeDataService {

  initialized: boolean = false
  trades: Trade[] = [];
  private apiURL: string = environment.apiUrl + '/trades/';

  constructor(private http: Http) {
  }

  // Simulate POST /trades
  addTrade(trade: Trade) {
    this.trades.push(trade);
    return this.http.post(this.apiURL, trade);
  }

  // Simulate GET /trades
  getAllTrades() {
    return this.http.get(this.apiURL)
        .map(
          response => response.json() as Trade[]
        );
  }

  // Simulate GET /trades
  getAllRates(currency) {
    return this.http.get(environment.apiUrl + '/rates/' + currency)
        .map(response => response.json())
        .map(ratesObj => ratesObj.rates);
  }

}