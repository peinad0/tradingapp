import {Trade} from './trade';

describe('Trade', () => {
  it('should create an instance', () => {
    expect(new Trade()).toBeTruthy();
  });
  it('should accept values in the constructor', () => {
    let trade = new Trade({
      sell_currency: 'USD',
      buy_currency: 'EUR',
      sell_amount: 100,
      buy_amount: 80,
      rate: 0.8,
    });
    expect(trade.sell_currency).toEqual('USD');
    expect(trade.buy_currency).toEqual('EUR');
    expect(trade.sell_amount).toEqual(100);
    expect(trade.buy_amount).toEqual(80);
    expect(trade.rate).toEqual(0.8);
  });
});
