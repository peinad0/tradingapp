import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule, Headers, BaseRequestOptions, RequestOptions, XSRFStrategy, CookieXSRFStrategy }    from '@angular/http';
import { CookieService, CookieOptions } from 'angular2-cookie/core';
import { HttpClient} from '@angular/common/http';



@Injectable()
export class DefaultRequestOptions extends BaseRequestOptions {
    headers: Headers = new Headers({
        'Content-Type': 'application/json'
    });
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        HttpModule
    ],
    providers: [
        CookieService,
        {provide: CookieOptions, useValue: {}},
        {
            provide: RequestOptions,
            useClass: DefaultRequestOptions
        },
        {provide: CookieService, useFactory: () => {return new CookieService();}},
        {
            provide: XSRFStrategy,
            useFactory: (cookieService) => {
                return new CookieXSRFStrategy('csrftoken', 'X-CSRFToken');
            },
            deps: [CookieService]
        },
        HttpClientModule,
        HttpClient
    ],
    bootstrap: [AppComponent]
})

export class AppModule {}
