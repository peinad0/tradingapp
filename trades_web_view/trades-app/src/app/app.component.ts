import { Component } from '@angular/core';
import { Trade } from './trade';
import { TradeDataService } from './trade-data.service';
import { Observable } from 'rxjs/Observable'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TradeDataService]
})

export class AppComponent {
  newTrade: Trade = new Trade();
  toggleForm: boolean = false;
  trades: Trade[]
  buyRates: any
  sellRates: any
  sellCurrencies: String[]
  buyCurrencies: String[]

  constructor(private tradeDataService: TradeDataService) {
    this.trades = []
    this.buyRates = {}
    this.sellRates = {}
    this.tradeDataService.getAllTrades().subscribe(trades => this.trades = trades);
    this.tradeDataService.getAllRates('EUR').subscribe(rates => {
        this.sellRates = rates
        this.sellCurrencies = Object.keys(rates)
        this.sellCurrencies.push('EUR')
        this.sellCurrencies.sort((a, b) => {
          var nameA=a.toLowerCase(), nameB=b.toLowerCase();
          if (nameA < nameB) //sort string ascending
            return -1;
          if (nameA > nameB)
            return 1;
          return 0; //default return value (no sorting)
        });
    });
  }

  addTrade() {
    this.tradeDataService.addTrade(this.newTrade)
        .subscribe(() => {
            this.tradeDataService.getAllTrades().subscribe(trades => this.trades = trades);
        })
    this.newTrade = new Trade();
    this.toggleForm = false;
  }

  getRates(currency){
    this.tradeDataService.getAllRates(currency).subscribe(rates => {
        this.buyRates = rates
        this.buyCurrencies = Object.keys(rates)
    })
  }

  computeAmount(rate) {
    if(this.buyRates && this.newTrade.sell_amount) {
        this.newTrade.rate = this.buyRates[rate]
        this.newTrade.buy_amount = this.newTrade.rate * this.newTrade.sell_amount
    }
  }
}
