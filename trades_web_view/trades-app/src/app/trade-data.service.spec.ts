import { TestBed, inject } from '@angular/core/testing';
import { Trade } from './trade';
import { TradeDataService } from './trade-data.service';

describe('TradeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TradeDataService]
    });
  });
});