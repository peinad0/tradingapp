export const environment = {
  production: true,
  apiUrl: 'https://currency-trading-app.herokuapp.com/api/v1'
};
